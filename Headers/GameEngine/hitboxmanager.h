#ifndef HITBOXMANAGER_H
#define HITBOXMANAGER_H

#include <QMap>
#include "hitbox.h"

class HitBoxManager
{
public:

    static int lastIndex;
    static QMap<int, HitBox*> mapHitbox;

    static int registerHitbox(HitBox* hitbox);

    static void removeHitbox(int key);

    static bool isTouching(HitBox* hitbox);

    static bool isTouching(HitBox* hitbox, QVector<HitBox*>* vectRetour);


private :

    HitBoxManager() {}

};

#endif // HITBOXMANAGER_H
