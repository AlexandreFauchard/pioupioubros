#ifndef HITBOX_H
#define HITBOX_H

#include <QObject>
#include <QPen>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QTimer>

class HitBox : public QGraphicsRectItem
{
public:
    HitBox(QGraphicsItem* p, int x, int y, int w, int h, QString c);
    ~HitBox();

    bool isTouch(int x1, int y1, int x2, int y2);
    bool isTouching();
    bool isTouching(QVector<HitBox*>* vectRetour);

    int getRelativeX() const;
    void setRelativeX(int value);

    int getRelativeY() const;
    void setRelativeY(int value);

    int getWidth() const;
    void setWidth(int value);

    int getHeight() const;
    void setHeight(int value);

    int getX1();
    int getY1();
    int getX2();
    int getY2();

    void refresh();


    QPen getQPen() const;
    void setQPen(const QPen &value);

    QString getClasse() const;
    void setClasse(const QString &value);

    void lauch();

    QTimer *getActionneur() const;
    void setActionneur(QTimer *value);

private:
    QGraphicsItem* parent;

    QPen qPen;

    int relativeX;
    int relativeY;

    int width;
    int height;

    QString classe;

    int keyManager;

    QTimer* actionneur;
};

#include "hitboxmanager.h"
#endif // HITBOX_H
