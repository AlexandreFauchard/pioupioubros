#ifndef PIOUPIOU_H
#define PIOUPIOU_H

#define SPRITE_PIOUPIOU_PATH ":/Sprites/Player/PiouPiou_%1_%2.png"

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItemGroup>
#include <QPropertyAnimation>
#include "../GameEngine/hitbox.h"

class PiouPiou : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    Q_PROPERTY(qreal y READ y WRITE setY)
public:
    explicit PiouPiou();
    ~PiouPiou();

    qreal y() const;

    void jump();
    void moveRight();
    void moveLeft();
    void stop(bool right);
    void updatePixmap(bool right);

    qreal getXPos();
    qreal getYPos();

signals:


public slots:

    void setY(qreal y);

private :
    int numSprite = 1;

    QGraphicsPixmapItem* sprite;

    qreal m_y;
    qreal scale;
    qreal posYSol;
    bool aSaute = true;
    bool chuteEnCours = true;
    qreal puissanceSaut;
    qreal vitesseDeplacement;

    QPropertyAnimation* animationChuteY;
    QPropertyAnimation* animationSautY;

    HitBox* hitboxTete;
    HitBox* hitboxCorpsDroite;
    HitBox* hitboxCorpsGauche;
    HitBox* hitboxPied;
};

#endif // PIOUPIOU_H
