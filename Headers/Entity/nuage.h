#ifndef NUAGE_H
#define NUAGE_H

#define SPRITE_NUAGE_PATH ":/Tiles/Nuage.png"
#define SPRITE_NUAGE_MAGIQUE_PATH ":/Tiles/Nuage_Magique.png"

#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>


class Nuage : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX)
public:
    explicit Nuage();
    ~Nuage();

    qreal x() const;
    void startAnim();

signals:

public slots :

    void setX(qreal x);

private :
    QGraphicsPixmapItem* sprite;
    QPropertyAnimation* xAnimation;
    int yPos;
    qreal scale;
    int vitesseDeplacement;


    qreal m_x;
};



#endif // NUAGE_H
