#ifndef WITCH_H
#define WITCH_H

#define SPRITE_WITCH_PATH ":/Sprites/Witch/Mage_0.png"

#include <QObject>
#include <QGraphicsItemGroup>
#include <QGraphicsScene>
#include <QTimer>

#include "../GameEngine/hitbox.h"

class Witch : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
public:
    explicit Witch(qreal startX, qreal startY);
    ~Witch();

signals:

private :
    QGraphicsPixmapItem* sprite;

    qreal scale;
    qreal x;
    qreal y;
    qreal direction;
    qreal posYSol;
    int scaleX;

    HitBox* hitbox;

    QTimer* timerMove;

    int m_numSprite;
};

#endif // WITCH_H
