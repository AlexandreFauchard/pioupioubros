#ifndef BALLE_H
#define BALLE_H

#define SPRITE_BALLE_PATH ":/Sprites/Chasseur/Balle.png"

#include <QObject>
#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QEasingCurve>
#include <QGraphicsScene>
#include <QDebug>
#include <QTimer>

#include "../GameEngine/hitbox.h"

class Balle : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT

public:
    explicit Balle(QObject* p, qreal x, qreal y, int multiplicateurVitesseDeplacement);
    ~Balle();

public slots:

signals:

private :

    QGraphicsPixmapItem* sprite;
    qreal scale;
    qreal vitesseDeplacement;

    qreal xPos;
    qreal yPos;

    QTimer* timerDeplacement;

    HitBox* hitbox;

    QObject* parent;

};

#include "chasseur.h"

#endif // BALLE_H
