#ifndef MONSTER_H
#define MONSTER_H

#define SPRITE_MONSTER_PATH ":/Sprites/Monster.png"

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItemGroup>
#include <QDebug>
#include <QTimer>
#include <QPropertyAnimation>

#include "../GameEngine/hitbox.h"

class Monster : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    Q_PROPERTY(int numSprite READ numSprite WRITE setNumSprite)
public:
    explicit Monster(qreal startX, qreal startY, qreal endX);
    ~Monster();

    int numSprite() const;
public slots:
    void setNumSprite(int numSprite);
private :

    QGraphicsPixmapItem* sprite;

    qreal scale;
    qreal x;
    qreal y;
    qreal direction;
    qreal posYSol;
    int scaleX;

    HitBox* hitbox;

    QTimer* timerMove;

    int m_numSprite;
};


#endif // MONSTER_H
