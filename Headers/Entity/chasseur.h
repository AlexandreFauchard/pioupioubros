#ifndef CHASSEUR_H
#define CHASSEUR_H

#define SPRITE_CHASSEUR_PATH ":/Sprites/Chasseur/Chasseur_%1.png"

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItemGroup>
#include <QDebug>
#include <QTimer>
#include <QPropertyAnimation>

#include "../GameEngine/hitbox.h"
#include "balle.h"

class Chasseur : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    Q_PROPERTY(int numSprite READ numSprite WRITE setNumSprite)
public:
    explicit Chasseur(qreal startX, qreal startY);
    ~Chasseur();

    int numSprite() const;

    void addProjectile(Balle* b);

    void deleteProjectile(Balle* b);

    void death();

public slots:
    void setNumSprite(int numSprite);

signals:


private :

    QGraphicsPixmapItem* sprite;

    qreal scale;
    qreal posYSol;
    int scaleX;

    HitBox* hitboxTete;
    HitBox* hitboxCorps;

    QTimer* timerCheckSide;
    QTimer* timerShoot;

    QPropertyAnimation* animationTir;
    QPropertyAnimation* animationMort;
    int m_numSprite;

    QVector<Balle*> tabProjectiles;
};

#endif // CHASSEUR_H
