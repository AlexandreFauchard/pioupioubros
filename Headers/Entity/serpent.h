#ifndef SERPENT_H
#define SERPENT_H

#define SPRITE_SERPENT_PATH ":/Sprites/Serpent/"

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItemGroup>
#include <QDebug>
#include <QTimer>
#include <QPropertyAnimation>

#include "../GameEngine/hitbox.h"

class Serpent : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    Q_PROPERTY(int numSprite READ numSprite WRITE setNumSprite)
public:
    explicit Serpent(qreal startX, qreal startY, qreal endX);
    ~Serpent();

    int numSprite() const;
public slots:
    void setNumSprite(int numSprite)
    {
        m_numSprite = numSprite;
    }

private :

    QGraphicsPixmapItem* sprite;

    qreal scale;
    qreal x;
    qreal y;
    qreal direction;
    qreal posYSol;
    int scaleX;

    HitBox* hitbox;

    QTimer* timerMove;
    QTimer* timerSprite;

    int m_numSprite;
    void changeSprite();
};


#endif // SERPENT_H
