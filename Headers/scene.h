#ifndef SCENE_H
#define SCENE_H

#define BG_CIEL_PATH ":/Tiles/Ciel.png"
#define UI_DEATH_SCREEN_PATH ":/UI/EcranMort.png"
#define UI_WIN_SCREEN_PATH ":/UI/EcranWin.png"

#include <QGraphicsScene>

#include "./Entity/nuage.h"
#include "./Entity/pioupiou.h"
#include "./Entity/balle.h"
#include "./Entity/chasseur.h"
#include "./Entity/monster.h"
#include "./Entity/serpent.h"
#include "./Entity/witch.h"
#include "./Entity/balle.h"

class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit Scene(QObject *parent = nullptr);

    void StartGame();

    int getNbNuage(){
        return nbNuages;
    }

    QVector<Nuage*> getNuageTab(){
        return tabNuage;
    }

    void addChasseur(Chasseur* c);

    void deleteChasseur(Chasseur* c);

    void addWhitch(Witch* c);

    void deleteWhitch(Witch* c);

    void addMonster(Monster* c);

    void deleteMonster(Monster* c);

    void addSerpent(Serpent* c);

    void deleteSerpent(Serpent* c);

    void perdu();

    void win();

    PiouPiou *getJoueur() const;

signals:

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private :
    bool partieFinie;
    void cheatCodeActive();
    PiouPiou* joueur;
    QTimer* moveRightTimer;
    QTimer* moveLeftTimer;
    int nbNuages;
    //Niveau* niveau;
    QVector<Nuage*> tabNuage;
    QVector<Chasseur*> tabChasseurs;
    QVector<Witch*> tabWhitches;
    QVector<Monster*> tabMonsters;
    QVector<Serpent*> tabSerpents;

    QVector<int> konamiCodeInput;
    QVector<int> konamiCode = {Qt::Key_Up, Qt::Key_Up, Qt::Key_Down, Qt::Key_Down, Qt::Key_Left, Qt::Key_Right, Qt::Key_Left, Qt::Key_Right, Qt::Key_B, Qt::Key_A};
    QObject* widgetParent;

    bool mortel;
};
#include "./niveau.h"

#endif // SCENE_H
