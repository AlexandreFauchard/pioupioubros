#ifndef NIVEAU_H
#define NIVEAU_H

#define TILE_SOL_PATH ":/Tiles/Sol.png"

#include <QObject>
#include <QGraphicsItemGroup>
#include <QGraphicsRectItem>
#include "./GameEngine/hitbox.h"
#include "scene.h"

class Niveau : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
public:
    explicit Niveau(Scene* s);

signals:


private :
    void additionToGroupe(char const *url, int pixelX, int pixelY, int longeur, int hauteur);
    void additionToGroupeAvecClasse(char const *url, int pixelX, int pixelY, int longeur, int hauteur, QString classe);
};

#endif // NIVEAU_H
