QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ./Sources/Entity/balle.cpp \
    ./Sources/Entity/chasseur.cpp \
    ./Sources/Entity/nuage.cpp \
	./Sources/Entity/pioupiou.cpp \
	./Sources/GameEngine/hitbox.cpp \
    ./Sources/GameEngine/hitboxmanager.cpp \
    ./Sources/niveau.cpp \
	./Sources/scene.cpp \
	./Sources/widget.cpp \
   ./Sources/Entity/monster.cpp \
    Sources/Entity/serpent.cpp \
    Sources/Entity/witch.cpp \
	main.cpp \

HEADERS += \
	./Headers/Entity/balle.h \
    ./Headers/Entity/chasseur.h \
    ./Headers/Entity/nuage.h \
	./Headers/Entity/pioupiou.h \
	./Headers/GameEngine/hitbox.h \
    ./Headers/GameEngine/hitboxmanager.h \
    ./Headers/niveau.h \
	./Headers/scene.h \
	./Headers/widget.h \ \
    ./Headers/Entity/monster.h \
    Headers/Entity/serpent.h \
    Headers/Entity/witch.h

FORMS += \
    widget.ui

DESTDIR  = ./Build/

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Ressources/Sprites/Sprites.qrc \
    Ressources/Tiles/Tiles.qrc \
    Ressources/UI/UI.qrc
