#include "../Headers/niveau.h"
#include "../Headers/Entity/serpent.h"
#include "../Headers/widget.h"

#include <QBrush>
#include <QStyleOptionGraphicsItem>
Niveau::Niveau(Scene* s)
{
    //Le niveau doit faire 15 600 pixels de long (taille définit à partir d'un niveau de taille moyenne de Mario et da la longueur du saut du PiouPiou)
    //Le niveau commence à -1000 pixels, les troues font 300 pixels de long
    char const *sol = ":/Tiles/Sol.png";
    char const *block = ":/Tiles/Block2.png";
    char const *block2 = ":/Tiles/Block2.png";
    char const *tuyauFin = ":/Tiles/Fin.png";
    char const *drapeauFin = ":/Tiles/Flag.png";
    this->additionToGroupe(sol, -1000,0,2000,150);
    Scene* sceneMain = s;

    Serpent* s1 = new Serpent(200, 0, 600-100);
    sceneMain->addSerpent(s1);
    addToGroup(s1);


    //Elévation -> Début : 600, longueur : 100, hauteur : 100
    this->additionToGroupe(block, 600,-100,200,100);
    this->additionToGroupe(block, 700,-200,100,100);



    //Premier trou -> 1000 à 1300


    //Deuxième sol -> Début : 1300, longeur : 3700
    this->additionToGroupe(sol, 1300,0,3700,150);

    Chasseur* c1 = new Chasseur(1500, 0);
    addToGroup(c1);
    sceneMain->addChasseur(c1);

    //Elévation -> Début : 1600, de 7 blocs
    this->additionToGroupe(block, 1600,-100,100,100);
    this->additionToGroupe(block, 1700,-100,100,100);
    this->additionToGroupe(block, 1700,-200,100,100);
    this->additionToGroupe(block, 1800,-100,100,100);
    this->additionToGroupe(block, 1800,-200,100,100);
    this->additionToGroupe(block, 1800,-300,100,100);
    this->additionToGroupe(block, 1900,-100,100,100);

    Serpent* s2 = new Serpent(2000, 0, 2700-100);
    addToGroup(s2);
    sceneMain->addSerpent(s2);


    //Elévation en l'air -> Début : 2700, de 4 blocs
    this->additionToGroupe(block2, 2700,-150,100,20);
    this->additionToGroupe(block2, 2800,-150,100,20);
    this->additionToGroupe(block2, 2900,-150,100,20);
    this->additionToGroupe(block2, 3000,-150,100,20);

    Witch* w2 = new Witch(3100, -100);
    addToGroup(w2);
    sceneMain->addWhitch(w2);

    //Elévation en l'air -> Début : 3300, de 4 blocs
    this->additionToGroupe(block2, 3300,-200,100,20);
    this->additionToGroupe(block2, 3400,-200,100,20);
    this->additionToGroupe(block2, 3500,-200,100,20);
    this->additionToGroupe(block, 3600,-100,100,100);
    this->additionToGroupe(block, 3600,-200,100,100);

    Chasseur* c2 = new Chasseur(4600, 0);
    addToGroup(c2);
    sceneMain->addChasseur(c2);

    //Elévation -> Début : 4700, de 7 blocs
    this->additionToGroupe(block, 4700,-100,100,100);
    this->additionToGroupe(block, 4800,-100,100,100);
    this->additionToGroupe(block, 4800,-200,100,100);
    this->additionToGroupe(block, 4900,-100,100,100);
    this->additionToGroupe(block, 4900,-200,100,100);
    this->additionToGroupe(block, 4900,-300,100,100);



    //Deuxième trou -> 5000 à 5300



    //Troisième sol -> Début : 5300, longeur : 1700
    this->additionToGroupe(sol, 5300,0,1700,150);

    //Elévation -> Début : 5300, de 7 blocs
    this->additionToGroupe(block, 5300,-100,100,100);
    this->additionToGroupe(block, 5300,-200,100,100);
    this->additionToGroupe(block, 5300,-300,100,100);
    this->additionToGroupe(block, 5400,-100,100,100);
    this->additionToGroupe(block, 5400,-200,100,100);
    this->additionToGroupe(block, 5500,-100,100,100);

    Serpent* s3 = new Serpent(5600, 0, 6300-100);
    addToGroup(s3);
    sceneMain->addSerpent(s3);

    //Elévation en l'air -> Début : 6300, de 4 blocs
    this->additionToGroupe(block2, 6300,-150,100,20);
    this->additionToGroupe(block2, 6400,-150,100,20);
    this->additionToGroupe(block2, 6500,-150,100,20);
    this->additionToGroupe(block2, 6600,-150,100,20);

    Witch* w3 = new Witch(6800, -100);
    addToGroup(w3);
    sceneMain->addWhitch(w3);

    //Elévation -> Début : 6900, de 1 blocs
    this->additionToGroupe(block, 6900,-100,100,100);




    //Troisième trou -> 7000 à 7300



    //Quatrième sol -> Début : 7300, longeur : 3700
    this->additionToGroupe(sol, 7300,0,1400,150);

    //Elévation -> Début : 7300, de 1 blocs
    this->additionToGroupe(block, 7300,-100,100,100);

    Chasseur* c3 = new Chasseur(7800, 0);
    addToGroup(c3);
    sceneMain->addChasseur(c3);

    //Elévation -> Début : 7900, de 3 blocs
    this->additionToGroupe(block, 7900,-100,100,100);
    this->additionToGroupe(block, 8000,-100,100,100);
    this->additionToGroupe(block, 8000,-200,100,100);

    Serpent* s4 = new Serpent(8100, 0, 8700-100);
    addToGroup(s4);
    sceneMain->addSerpent(s4);



    //Quatrième trou -> 8700 à 9600




    //Elévation en l'air -> Début : 8800, de 5 blocs
    this->additionToGroupe(block2, 8800,-150,100,20);
    this->additionToGroupe(block2, 8900,-150,100,20);
    this->additionToGroupe(block2, 9000,-150,100,20);
    this->additionToGroupe(block2, 9100,-150,100,20);
    this->additionToGroupe(block2, 9200,-150,100,20);

    //Quatrième sol -> Début : 7300, longeur : 3700
    this->additionToGroupe(sol, 9600,0,1400,150);

    //Elévation en l'air -> Début : 9700, de 3 blocs
    this->additionToGroupe(block2, 9700,-150,100,20);
    this->additionToGroupe(block2, 9800,-150,100,20);
    this->additionToGroupe(block2, 9900,-150,100,20);

    Witch* w4 = new Witch(10100, -100);
    addToGroup(w4);
    sceneMain->addWhitch(w4);

    //Elévation -> Début : 10200, de 6 blocs
    this->additionToGroupe(block2, 10200,-200,100,20);
    this->additionToGroupe(block2, 10300,-200,100,20);
    this->additionToGroupe(block2, 10400,-200,100,20);
    this->additionToGroupe(block, 10500,-100,100,100);
    this->additionToGroupe(block, 10500,-200,100,100);

    //Elévation qui bloque -> Début : 10900, de 3 blocs
    this->additionToGroupe(block, 10700,-100,100,100);
    this->additionToGroupe(block, 10800,-100,100,100);
    this->additionToGroupe(block, 10800,-200,100,100);
    this->additionToGroupe(block, 10900,-200,100,100);
    this->additionToGroupe(block, 10900,-100,100,100);
    this->additionToGroupe(block, 10900,-300,100,100);




    //Cinquième trou -> 11000 à 11300





    //Cinquième sol -> Début : 11300, longeur : 1700
    this->additionToGroupe(sol, 11300,0,1700,150);

    //Elévation pour atterir -> Début : 5300, de 7 blocs
    this->additionToGroupe(block, 11300,-100,100,100);
    this->additionToGroupe(block, 11300,-200,100,100);
    this->additionToGroupe(block, 11300,-300,100,100);
    this->additionToGroupe(block, 11400,-100,100,100);
    this->additionToGroupe(block, 11400,-200,100,100);
    this->additionToGroupe(block, 11500,-100,100,100);

    Serpent* s5 = new Serpent(11600, 0, 12200-100);
    addToGroup(s5);
    sceneMain->addSerpent(s5);

    //Elévation -> Début : 12200, de 7 blocs
    this->additionToGroupe(block, 12200,-100,100,100);
    this->additionToGroupe(block, 12300,-100,100,100);
    this->additionToGroupe(block, 12300,-200,100,100);
    this->additionToGroupe(block, 12400,-100,100,100);
    this->additionToGroupe(block, 12400,-200,100,100);
    this->additionToGroupe(block, 12400,-300,100,100);
    this->additionToGroupe(block, 12500,-100,100,100);




    //Sixième trou -> 13000 à 13300





    //Sixième sol -> Début : 13300, longeur : 1300 + 3700 pour fin du niveau
    this->additionToGroupe(sol, 13300,0,5000,150);

    Chasseur* c4 = new Chasseur(13500, 0);
    addToGroup(c4);
    sceneMain->addChasseur(c4);

    //Elévation en l'air -> Début : 13600, de 3 blocs
    this->additionToGroupe(block2, 13600,-150,100,20);
    this->additionToGroupe(block2, 13700,-150,100,20);
    this->additionToGroupe(block2, 13800,-150,100,20);

    additionToGroupeAvecClasse(drapeauFin, 14016, -400, 64, 400, "Fin");
    additionToGroupe(tuyauFin, 14016, -150, 64, 150);
};


void Niveau::additionToGroupe(char const *url, int pixelX, int pixelY, int longueur, int hauteur){
    QGraphicsRectItem* item = new QGraphicsRectItem(pixelX,pixelY,longueur,hauteur);

    QPixmap sprite(url);
    int longueurSprite = sprite.size().width();
    if(longueur < longueurSprite){
        longueurSprite = longueur;
    }
    sprite = sprite.scaled(longueur, hauteur);
    int hauteurSprite = sprite.size().height();
    if(hauteur < hauteurSprite){
        hauteurSprite = hauteur;
    }
    sprite = sprite.scaled(longueurSprite, hauteurSprite);

    item->setBrush(QBrush(sprite));
    item->setPen(Qt::NoPen);
    addToGroup(item);
    HitBox* box = new HitBox(item, item->rect().x(), item->rect().y(),item->rect().width(),item->rect().height(), "Sol");
    addToGroup(box);
};


void Niveau::additionToGroupeAvecClasse(char const *url, int pixelX, int pixelY, int longueur, int hauteur, QString classe){
    QGraphicsRectItem* item = new QGraphicsRectItem(pixelX,pixelY,longueur,hauteur);

    QPixmap sprite(url);
    int longueurSprite = sprite.size().width();
    if(longueur < longueurSprite){
        longueurSprite = longueur;
    }
    sprite = sprite.scaled(longueur, hauteur);
    int hauteurSprite = sprite.size().height();
    if(hauteur < hauteurSprite){
        hauteurSprite = hauteur;
    }
    sprite = sprite.scaled(longueurSprite, hauteurSprite);

    item->setBrush(QBrush(sprite));
    item->setPen(Qt::NoPen);
    addToGroup(item);

    HitBox* box = new HitBox(item, item->rect().x(), item->rect().y(),item->rect().width(),item->rect().height(), classe);
    addToGroup(box);
};
