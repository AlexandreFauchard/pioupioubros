#define SHOW_HITBOX 0

#include "../../Headers/GameEngine/hitbox.h"

#include <QPen>
#include <QDebug>

HitBox::HitBox(QGraphicsItem* p, int x, int y, int w, int h, QString c)
{

    classe = c;
    parent = p;
    relativeX = x;
    relativeY = y;
    width = w;
    height = h;

    qPen = QPen(Qt::red);
    qPen.setWidth(5);
    setPen(qPen);
    #if SHOW_HITBOX == 1
        setVisible(true);
    #else
        setVisible(false);
    #endif

    setRect(QRectF(parent->x() + relativeX, parent->y() + relativeY, width, height));

    keyManager = HitBoxManager::registerHitbox(this);

}

HitBox::~HitBox()
{

    HitBoxManager::removeHitbox(keyManager);
}

bool HitBox::isTouch(int objX1, int objY1, int objX2, int objY2)
{
    //On s'assure que les données sont dans le bon ordre
    if(objX1 > objX2){
        int a = objX1;
        objX1 = objX2;
        objX2 = a;
    }
    if(objY1 > objY2){
        int a = objY1;
        objY1 = objY2;
        objY2 = a;
    }

    //on calcule nos propre coordonnees
    int x1 = getX1();
    int y1 = getY1();
    int x2 = getX2();
    int y2 = getY2();

    //qInfo() << "(" << objX1 << ", " << objY1 << ", " << objX2 << ", " << objY2 << ") X" << "(" << x1 << ", " << y1 << ", " << x2 << ", " << y2 << ")";

    int result = 0;

    if(objY2 >= y1){
        result++;
    }
    if(objY1 <= y2){
        result++;
    }
    if(objX1 <= x2){
        result++;
    }
    if(objX2 >= x1){
        result++;
    }

    if(result < 4){ //Hors limite ?
        qPen.setColor(Qt::red);
        setPen(qPen);
        refresh();
        return false;
    }else{
        qPen.setColor(Qt::green);
        setPen(qPen);
        refresh();
        return true;
    }

}

bool HitBox::isTouching()
{
    bool result = HitBoxManager::isTouching(this);
    if(!result){
        qPen.setColor(Qt::red);
        setPen(qPen);
        refresh();
    }else{
        qPen.setColor(Qt::green);
        setPen(qPen);
        refresh();
    }
    return result;
}

bool HitBox::isTouching(QVector<HitBox *> *vectRetour)
{
    bool result = HitBoxManager::isTouching(this, vectRetour);
    if(!result){
        qPen.setColor(Qt::red);
        setPen(qPen);
        refresh();
    }else{
        qPen.setColor(Qt::green);
        setPen(qPen);
        refresh();
    }
    return result;
}

int HitBox::getRelativeX() const
{
    return relativeX;
}

void HitBox::setRelativeX(int value)
{
    relativeX = value;
}

int HitBox::getRelativeY() const
{
    return relativeY;
}

void HitBox::setRelativeY(int value)
{
    relativeY = value;
}

int HitBox::getWidth() const
{
    return width;
}

void HitBox::setWidth(int value)
{
    width = value;
}

int HitBox::getHeight() const
{
    return height;
}

void HitBox::setHeight(int value)
{
    height = value;
}

int HitBox::getX1()
{
    return parent->x() + relativeX;
}

int HitBox::getY1()
{
    return parent->y() + relativeY;
}

int HitBox::getX2()
{
    return parent->x() + relativeX + width;
}

int HitBox::getY2()
{
    return parent->y() + relativeY + height;
}

void HitBox::refresh()
{
    if(classe == "EnnemisA"){
        qInfo() << parent->x() + relativeX << " - " << parent->y() + relativeY;
    }
    setRect(QRectF(parent->x() + relativeX, parent->y() + relativeY, width, height));
}

QPen HitBox::getQPen() const
{
    return qPen;
}

void HitBox::setQPen(const QPen &value)
{
    qPen = value;
}

QString HitBox::getClasse() const
{
    return classe;
}

void HitBox::setClasse(const QString &value)
{
    classe = value;
}

void HitBox::lauch()
{
    if(actionneur != nullptr){
        actionneur->start();
    }
}

QTimer *HitBox::getActionneur() const
{
    return actionneur;
}

void HitBox::setActionneur(QTimer *value)
{
    actionneur = value;
}
