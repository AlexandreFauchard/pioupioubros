#include "../../Headers/GameEngine/hitboxmanager.h"

int HitBoxManager::lastIndex = 0;
QMap<int, HitBox*> HitBoxManager::mapHitbox;

int HitBoxManager::registerHitbox(HitBox* hitbox)
{
    int index = -1;
    //Trouve le premier index vide
    if(!HitBoxManager::lastIndex){
        HitBoxManager::lastIndex = 1;
        index = 0;
    }else{
        for(int i = 0; i < HitBoxManager::lastIndex; i++){
            if(!HitBoxManager::mapHitbox.contains(i)){
                index = i;
                break;
            }
        }
        if(index == -1){
            index = HitBoxManager::lastIndex;
            HitBoxManager::lastIndex++;
        }
    }

    //On sauvegarde le pointeur vers la hitbox
    HitBoxManager::mapHitbox[index] = hitbox;

    return index;
}

void HitBoxManager::removeHitbox(int key)
{
    HitBoxManager::mapHitbox.remove(key);
}

bool HitBoxManager::isTouching(HitBox* hitbox)
{

    int x1 = hitbox->getX1();
    int y1 = hitbox->getY1();
    int x2 = hitbox->getX2();
    int y2 = hitbox->getY2();

    for(int i = 0; i < HitBoxManager::lastIndex; i++){
        if(HitBoxManager::mapHitbox.contains(i)){//Est ce que l'id est assigné
            if(HitBoxManager::mapHitbox[i] != hitbox){
                if(HitBoxManager::mapHitbox[i]->isTouch(x1, y1, x2, y2)){
                    return true;
                }
            }
        }
    }

    return false;
}

bool HitBoxManager::isTouching(HitBox *hitbox, QVector<HitBox*>* vectRetour)
{
    int x1 = hitbox->getX1();
    int y1 = hitbox->getY1();
    int x2 = hitbox->getX2();
    int y2 = hitbox->getY2();

    bool result = false;

    for(int i = 0; i < HitBoxManager::lastIndex; i++){
        if(HitBoxManager::mapHitbox.contains(i)){//Est ce que l'id est assigné
            if(HitBoxManager::mapHitbox[i] != hitbox){
                if(HitBoxManager::mapHitbox[i]->isTouch(x1, y1, x2, y2)){
                    result = true;
                    vectRetour->append(HitBoxManager::mapHitbox[i]);
                }
            }
        }
    }

    return result;
}
