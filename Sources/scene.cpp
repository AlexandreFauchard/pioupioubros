#include "../Headers/scene.h"
#include "../Headers/widget.h"
#include "../Headers/Entity/serpent.h"

#include <QKeyEvent>
#include <QTimer>
#include <QDebug>
#include <QGraphicsTextItem>


Scene::Scene(QObject *parent) : QGraphicsScene(parent)
{
    mortel = true;

    widgetParent = parent;

    int nbNuages = 4;
    partieFinie = false;

    for(int i = 0; i < nbNuages; i++){
        Nuage* newNuage = new Nuage();
        tabNuage.append(newNuage);
        addItem(newNuage);
    }


    joueur = new PiouPiou();
    addItem(joueur);

    moveRightTimer = new QTimer(this);
    connect(moveRightTimer, &QTimer::timeout, [=] () {
        if(!partieFinie && joueur != nullptr){
            joueur->updatePixmap(true);
            joueur->moveRight();
        }
    });

    moveLeftTimer = new QTimer(this);
    connect(moveLeftTimer, &QTimer::timeout, [=] () {
        if(!partieFinie && joueur != nullptr){
            joueur->updatePixmap(false);
            joueur->moveLeft();
        }
    });


    //FOND
    QRadialGradient skyGradient(sceneRect().x() + sceneRect().size().width()/2.,0,sceneRect().size().width());
    skyGradient.setColorAt(0, QColor::fromRgbF(0.4,0.66,0.9,1));
    skyGradient.setColorAt(0.8, QColor::fromRgbF(0.4,0.66,0.9,1));
    skyGradient.setColorAt(1, QColor::fromRgbF(0.2,0.56,0.88,1));
    setBackgroundBrush(QBrush(QPixmap(BG_CIEL_PATH)));



    Niveau* niveau = new Niveau(this);
    addItem(niveau);

}

void Scene::addChasseur(Chasseur *c)
{
    tabChasseurs.append(c);
}

void Scene::deleteChasseur(Chasseur* c)
{
    removeItem(c);
    int index = tabChasseurs.indexOf(c);
    if(index >= 0){
        tabChasseurs.removeAt(index);
    }
}

void Scene::addWhitch(Witch *w)
{
    tabWhitches.append(w);
}

void Scene::deleteWhitch(Witch*w)
{
    removeItem(w);
    int index = tabWhitches.indexOf(w);
    if(index >= 0){
        tabWhitches.removeAt(index);
    }
}

void Scene::addMonster(Monster *m)
{
    tabMonsters.append(m);
}

void Scene::deleteMonster(Monster *m)
{
    removeItem(m);
    int index = tabMonsters.indexOf(m);
    if(index >= 0){
        tabMonsters.removeAt(index);
    }
}

void Scene::addSerpent(Serpent *s)
{
    tabSerpents.append(s);
}

void Scene::deleteSerpent(Serpent *s)
{
    removeItem(s);
    int index = tabSerpents.indexOf(s);
    if(index >= 0){
        tabSerpents.removeAt(index);
    }
}

void Scene::perdu()
{
    if(mortel){
        try {

            partieFinie = true;
            konamiCodeInput.clear();

            //moveRightTimer->stop();
            //moveLeftTimer->stop();
            //delete moveRightTimer;
            //delete moveLeftTimer;

            //delete niveau;

            for(int i = 0; i < tabChasseurs.length(); i++){
                delete tabChasseurs.at(i);
            }
            tabChasseurs.clear();

            for(int i = 0; i < tabWhitches.length(); i++){
                delete tabWhitches.at(i);
            }
            tabWhitches.clear();

            for(int i = 0; i < tabMonsters.length(); i++){
                delete tabMonsters.at(i);
            }
            tabMonsters.clear();

            for(int i = 0; i < tabSerpents.length(); i++){
                delete tabSerpents.at(i);
            }
            tabSerpents.clear();

            for(int i = 0; i < tabNuage.length(); i++){
                delete tabNuage.at(i);
            }
            tabNuage.clear();

            //delete joueur;

            QGraphicsPixmapItem* image = new QGraphicsPixmapItem(QPixmap(UI_DEATH_SCREEN_PATH));
            image->setPos(500 - image->boundingRect().width() + sceneRect().x(), -250 - image->boundingRect().height());
            image->setScale(2);
            addItem(image);
        }catch(int e){

        }
    }
}

void Scene::win()
{
    try {

        partieFinie = true;

        //moveRightTimer->stop();
        //moveLeftTimer->stop();
        //delete moveRightTimer;
        //delete moveLeftTimer;

        //delete niveau;

        for(int i = 0; i < tabChasseurs.length(); i++){
            delete tabChasseurs.at(i);
        }
        tabChasseurs.clear();

        for(int i = 0; i < tabWhitches.length(); i++){
            delete tabWhitches.at(i);
        }
        tabWhitches.clear();

        for(int i = 0; i < tabMonsters.length(); i++){
            delete tabMonsters.at(i);
        }
        tabMonsters.clear();

        for(int i = 0; i < tabSerpents.length(); i++){
            delete tabSerpents.at(i);
        }
        tabSerpents.clear();

        for(int i = 0; i < tabNuage.length(); i++){
            delete tabNuage.at(i);
        }
        tabNuage.clear();

        //delete joueur;

        QGraphicsPixmapItem* image = new QGraphicsPixmapItem(QPixmap(UI_WIN_SCREEN_PATH));
        image->setPos(500 - image->boundingRect().width() + sceneRect().x(), -250 - image->boundingRect().height());
        image->setScale(2);
        addItem(image);
    }catch(int e){

    }

}

void Scene::cheatCodeActive(){
    mortel = false;
}


void Scene::keyPressEvent(QKeyEvent *event)
{
    konamiCodeInput.append(event->key());
    if(konamiCodeInput.size() == konamiCode.size()){
        bool stateKonami = true;
        for (int i = 0; i < konamiCodeInput.size(); ++i) {
            if(konamiCodeInput.at(i)!=konamiCode.at(i)){
                stateKonami = false;
                break;
            }
        }

        if(stateKonami)
            cheatCodeActive();
    }

    if(event->key() == Qt::Key_Left){
        if(!moveLeftTimer->isActive() && !partieFinie){
            moveRightTimer->stop();
            moveLeftTimer->start(10);
        }
    }else if(event->key() == Qt::Key_Right && !partieFinie){
        if(!moveRightTimer->isActive()){
            moveLeftTimer->stop();
            moveRightTimer->start(10);
        }
    }

    if(event->key() == Qt::Key_Space && !partieFinie){
        joueur->jump();
    }
}

void Scene::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left && !partieFinie){
        moveLeftTimer->stop();
        joueur->stop(false);
    }else if(event->key() == Qt::Key_Right && !partieFinie){
        moveRightTimer->stop();
        joueur->stop(true);
    }else if(event->key() == Qt::Key_Space && partieFinie){
        ((Widget*) widgetParent)->RestartGame();
    }
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(partieFinie){
        ((Widget*) widgetParent)->RestartGame();
    }
}

PiouPiou *Scene::getJoueur() const
{
    return joueur;
}


