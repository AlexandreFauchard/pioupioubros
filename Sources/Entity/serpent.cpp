#include "../../Headers/Entity/serpent.h"
#include "../../Headers/scene.h"

#include <QtMath>


Serpent::Serpent(qreal startX, qreal startY, qreal endX) : x(startX), y(startY), direction(1)
{
    sprite = new QGraphicsPixmapItem(QPixmap(QString(SPRITE_SERPENT_PATH) + "Serpent_0.png"));
    sprite->setRotation(0);

    addToGroup(sprite);

    scale = 1;
    posYSol = -boundingRect().height() * scale;

    sprite->setScale(scale);
    sprite->setPos(x, y+posYSol);

    hitbox = new HitBox(sprite, 9, 40, sprite->boundingRect().size().width(), 40, "Ennemis");
    addToGroup(hitbox);

    sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
    scaleX = -1;

    timerMove = new QTimer(this);
    connect(timerMove, &QTimer::timeout, [=] () {
        try{
            if(x >= endX && direction == 0) {
                direction = 1;
                sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
            } else if(x <= startX && direction == 1) {
                direction = 0;
                sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
            }

            if(direction == 1) {
                x -= 2;
            } else {
                x += 2;
            }

            sprite->setPos(x, y+posYSol);
            hitbox->refresh();
            QVector<HitBox*>* touch = new QVector<HitBox*>();
            if(hitbox->isTouching(touch)){
                for (int i = 0; i < touch->size(); ++i) {
                    if(touch->at(i) != nullptr && touch->at(i)->getClasse() == "Joueur"){
                        ((Scene*) scene())->perdu();
                        //return;
                    }
                }
            }
        }catch(int e){

        }
    });
    timerMove->start(10);

    timerSprite = new QTimer(this);
    connect(timerSprite, &QTimer::timeout, [=] () {
        changeSprite();
    });
    timerSprite->start(50);

    m_numSprite = 0;
}

Serpent::~Serpent()
{
    delete hitbox;
    timerMove->stop();
    timerSprite->stop();
}

int Serpent::numSprite() const
{
    return m_numSprite;
}

void Serpent::changeSprite()
{
    if(m_numSprite == 0){
        m_numSprite = 1;
        sprite->setPixmap(QPixmap(QString(SPRITE_SERPENT_PATH) + "Serpent_0.png"));
    }else{
        m_numSprite = 0;
        sprite->setPixmap(QPixmap(QString(SPRITE_SERPENT_PATH) + "Serpent_1.png"));
    }
    if(direction == 0){
        sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
    }
}
