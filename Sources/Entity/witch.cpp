#include "../../Headers/Entity/witch.h"

Witch::Witch(qreal startX, qreal startY) : x(startX), y(startY), direction(1)
{
    sprite = new QGraphicsPixmapItem(QPixmap(QString(SPRITE_WITCH_PATH)));
    addToGroup(sprite);

    scale = 1;
    posYSol = -boundingRect().height() * scale;

    sprite->setScale(scale);
    sprite->setPos(x, y + -sprite->boundingRect().height() * scale);

    hitbox = new HitBox(sprite, 9, 5, 110, 110, "Ennemis");
    addToGroup(hitbox);

    sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
    scaleX = -1;

    timerMove = new QTimer(this);
    connect(timerMove, &QTimer::timeout, [=] () {
        if(y >= 0) {
            direction = 1;
        } else if(y <= -300) {
            direction = 0;
        }

        if(direction == 1) {
            y -= 2;
        } else {
            y += 2;
        }

        sprite->setPos(x, y + -sprite->boundingRect().height() * scale);
        hitbox->refresh();
    });
    timerMove->start(10);
}

Witch::~Witch()
{
    delete hitbox;
    timerMove->stop();
}
