#include "../../Headers/Entity/balle.h"
#include "../../Headers/scene.h"

Balle::Balle(QObject* p, qreal x, qreal y, int multiplicateurVitesseDeplacement)
{
    sprite = new QGraphicsPixmapItem(QPixmap(SPRITE_BALLE_PATH));
    addToGroup(sprite);

    xPos = x + 20;
    yPos = y + 90;
    vitesseDeplacement = 3 * multiplicateurVitesseDeplacement;
    scale = 0.25;
    parent = p;

    sprite->setScale(scale);
    sprite->setPos(xPos, yPos);

    ((Chasseur*) parent)->addProjectile(this);

    hitbox = new HitBox(sprite, 0, 0, 12, 12, "Ennemis");
    addToGroup(hitbox);



    timerDeplacement = new QTimer(this);
    connect(timerDeplacement, &QTimer::timeout, [=] () {
        if(xPos - vitesseDeplacement < scene()->sceneRect().x() - 50 - sprite->boundingRect().width()*scale){
            delete hitbox;
            timerDeplacement->stop();
            ((Chasseur*) parent)->deleteProjectile(this);
            delete this;
        }else{
            xPos -= vitesseDeplacement;
            sprite->setX(xPos - vitesseDeplacement);
            hitbox->refresh();
            QVector<HitBox*>* touch = new QVector<HitBox*>();
            if(hitbox->isTouching(touch)){
                for (int i = 0; i < touch->size(); ++i) {
                    if(touch->at(i)->getClasse() == "Joueur"){
                        ((Scene*) scene())->perdu();
                        //return;
                    }
                }
            }
        }
    });
    timerDeplacement->start(15);

}

Balle::~Balle()
{
    //delete hitbox;
    timerDeplacement->stop();
}
