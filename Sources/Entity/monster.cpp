#include "../../Headers/Entity/monster.h"
#include "../../Headers/scene.h"

#include <QtMath>


Monster::Monster(qreal startX, qreal startY, qreal endX) : x(startX), y(startY), direction(1)
{
    sprite = new QGraphicsPixmapItem(QPixmap(QString(SPRITE_MONSTER_PATH)));
    sprite->setRotation(90);

    addToGroup(sprite);

    scale = 1;
    posYSol = -boundingRect().height() * scale;

    sprite->setScale(scale);
    sprite->setPos(x, y+posYSol);

    hitbox = new HitBox(sprite, 9 - sprite->boundingRect().size().height(), 5, 70, sprite->boundingRect().size().width()/2, "Ennemis");
    addToGroup(hitbox);

    sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
    scaleX = -1;

    timerMove = new QTimer(this);
    connect(timerMove, &QTimer::timeout, [=] () {
        if(x >= endX) {
            direction = 1;
        } else if(x <= startX) {
            direction = 0;
        }

        if(direction == 1) {
            x -= 2;
        } else {
            x += 2;
        }

        sprite->setPos(x, y+posYSol);
        hitbox->refresh();
        QVector<HitBox*>* touch = new QVector<HitBox*>();
        if(hitbox->isTouching(touch)){
            for (int i = 0; i < touch->size(); ++i) {
                if(touch->at(i)->getClasse() == "Joueur"){
                    ((Scene*) scene())->perdu();
                    //return;
                }
            }
        }
    });
    timerMove->start(10);
}

Monster::~Monster()
{
    delete hitbox;
    timerMove->stop();
}

int Monster::numSprite() const
{
    return m_numSprite;
}

void Monster::setNumSprite(int numSprite)
{
}
