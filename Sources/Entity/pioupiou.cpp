#include "../../Headers/Entity/pioupiou.h"

#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>

#include "../../Headers/scene.h"

PiouPiou::PiouPiou()
{
    sprite = new QGraphicsPixmapItem(QPixmap(QString(SPRITE_PIOUPIOU_PATH).arg("Right").arg(0)));
    addToGroup(sprite);

    puissanceSaut = 150;
    vitesseDeplacement = 4;
    scale = 1;
    posYSol = -sprite->boundingRect().height() * scale;

    sprite->setY(-128);
    m_y = -128;

    sprite->setScale(scale);
    sprite->setPos(0,-sprite->boundingRect().height() * scale);

    hitboxTete = new HitBox(sprite, 22, 40, 84, 20, "Joueur");
    addToGroup(hitboxTete);
    hitboxCorpsDroite = new HitBox(sprite, 70, 70, 30, 20, "Joueur");
    addToGroup(hitboxCorpsDroite);
    hitboxCorpsGauche = new HitBox(sprite, 20, 70, 30, 20, "Joueur");
    addToGroup(hitboxCorpsGauche);
    hitboxPied = new HitBox(sprite, 44, 108, 40, 20, "Joueur");
    addToGroup(hitboxPied);


    animationSautY = new QPropertyAnimation(this, "y", this);

    animationChuteY = new QPropertyAnimation(this, "y", this);
    animationChuteY->setStartValue(-sprite->boundingRect().height() * scale);
    animationChuteY->setEndValue(-posYSol + 50);
    animationChuteY->setEasingCurve(QEasingCurve::InQuad);
    animationChuteY->setDuration(350);

    animationChuteY->start();

    connect(animationChuteY, &QPropertyAnimation::finished, [=] () {
        QVector<HitBox*>* touch = new QVector<HitBox*>();
        if(!hitboxPied->isTouching(touch)){
            chuteEnCours = true;
            animationChuteY->setEndValue(50 - posYSol);
            animationChuteY->setStartValue(y());
            animationChuteY->setEasingCurve(QEasingCurve::Linear);
            animationChuteY->start();
        }else{
            //setY(sprite->y());

            animationChuteY->stop();
            aSaute = false;
            chuteEnCours = false;
            int minY = 9999;
            bool rebond = false;
            for (int i = 0; i < touch->size(); ++i) {
                if(touch->at(i)->getClasse() == "Rebond"){
                    rebond = true;
                    touch->at(i)->lauch();
                }
                if (touch->at(i)->getClasse() != "Perso"){
                    if(touch->at(i)->getY1() < minY){
                        minY = touch->at(i)->getY1();
                    }
                }
            }
            if(minY < 1000){
                sprite->setY(minY + posYSol);
            }else{
                sprite->setY(50 - posYSol);
            }

            if(rebond){
                jump();
                aSaute = true;
                chuteEnCours = true;
            }
        }
    });


}

PiouPiou::~PiouPiou()
{
    animationSautY->stop();
    animationChuteY->stop();
}

qreal PiouPiou::y() const
{
    return m_y;
}

void PiouPiou::jump()
{
    if(!aSaute){
        animationChuteY->stop();

        qreal curPosY = y();

        animationChuteY->setEndValue(curPosY);

        animationSautY->setStartValue(curPosY);
        animationSautY->setEndValue(curPosY - puissanceSaut);
        animationSautY->setEasingCurve(QEasingCurve::OutQuad);
        animationSautY->setDuration(350);

        animationSautY->start();
        aSaute = true;
        chuteEnCours = false;

        connect(animationSautY, &QPropertyAnimation::finished, [=] () {
            chuteEnCours = true;
            animationChuteY->setStartValue(y());
            animationChuteY->setEasingCurve(QEasingCurve::InQuad);
            animationChuteY->start();
        });
    }
}

void PiouPiou::moveRight()
{

    QVector<HitBox*>* touch = new QVector<HitBox*>();
    if(x() + vitesseDeplacement < 1000000 && !hitboxCorpsDroite->isTouching(touch)){
        sprite->moveBy(vitesseDeplacement, 0);

        hitboxTete->refresh();
        hitboxCorpsDroite->refresh();
        hitboxCorpsGauche->refresh();
        hitboxPied->refresh();

        if(scene()->sceneRect().x() + scene()->sceneRect().width() - boundingRect().width() * scale - 275 < sprite->x()){
            scene()->setSceneRect(
                        scene()->sceneRect().x() + vitesseDeplacement,
                        scene()->sceneRect().y(),
                        scene()->sceneRect().width(),
                        scene()->sceneRect().height()
                );
        }
    }else{
        for (int i = 0; i < touch->size(); ++i) {
            if(touch->at(i)->getClasse() == "Ennemis"){
                ((Scene*) scene())->perdu();
                return;
            }else if(touch->at(i)->getClasse() == "Fin"){
                ((Scene*) scene())->win();
                return;
            }
        }
    }
    if(!hitboxPied->isTouching() && !aSaute){
        aSaute = true;
        chuteEnCours = true;
        animationChuteY->setEndValue(50 - posYSol);
        animationChuteY->setStartValue(y());
        animationChuteY->setEasingCurve(QEasingCurve::InQuad);
        animationChuteY->start();
    }
}

void PiouPiou::moveLeft()
{

    QVector<HitBox*>* touch = new QVector<HitBox*>();
    if(sprite->x() - vitesseDeplacement > 0 && !hitboxCorpsGauche->isTouching(touch)){
        sprite->moveBy(-vitesseDeplacement, 0);

        hitboxTete->refresh();
        hitboxCorpsDroite->refresh();
        hitboxCorpsGauche->refresh();
        hitboxPied->refresh();

        if(sprite->x() > 150 && scene()->sceneRect().x() + 150 > sprite->x()){
            scene()->setSceneRect(
                        scene()->sceneRect().x() - vitesseDeplacement,
                        scene()->sceneRect().y(),
                        scene()->sceneRect().width(),
                        scene()->sceneRect().height()
                );
        }
    }else{
        qInfo() << "touch size " << touch->size();
        qInfo() << "touch size " << touch->length();
        for (int i = 0; i < touch->size(); ++i) {
            qInfo() << "touch " << i << " - " << touch->at(i)->getClasse();
            if(touch->at(i)->getClasse() == "Ennemis"){
                ((Scene*) scene())->perdu();
                return;
            }else if(touch->at(i)->getClasse() == "Fin"){
                ((Scene*) scene())->win();
                return;
            }
        }
    }
    if(!hitboxPied->isTouching() && !aSaute){
        aSaute = true;
        chuteEnCours = true;
        animationChuteY->setEndValue(50 - posYSol);
        animationChuteY->setStartValue(y());
        animationChuteY->setEasingCurve(QEasingCurve::InQuad);
        animationChuteY->start();
    }
}


void PiouPiou::stop(bool right)
{
    QString direction = right ? "Right" : "Left";
    sprite->setPixmap(QPixmap(QString(SPRITE_PIOUPIOU_PATH).arg(direction).arg(0)));
}

void PiouPiou::setY(qreal y)
{
    if(chuteEnCours){

        QVector<HitBox*>* touch = new QVector<HitBox*>();
        if(!hitboxPied->isTouching(touch)){
            sprite->moveBy(0, y - m_y);
            m_y = y;
        }else{
            animationChuteY->stop();
            aSaute = false;
            chuteEnCours = false;
            int minY = 9999;
            bool rebond = false;
            for (int i = 0; i < touch->size(); ++i) {
                if(touch->at(i)->getClasse() == "Rebond"){
                    rebond = true;
                    touch->at(i)->lauch();
                }
                if (touch->at(i)->getClasse() != "Perso"){
                    if(touch->at(i)->getY1() < minY){
                        minY = touch->at(i)->getY1();
                    }
                }
            }

            if(minY < 1000){
                sprite->setY(minY + posYSol);
            }else{
                sprite->setY(50 - posYSol);
            }

            if(rebond){
                jump();
                aSaute = true;
                chuteEnCours = true;
            }
        }
    }else{
        sprite->moveBy(0, y - m_y);
        m_y = y;
    }

    hitboxTete->refresh();
    hitboxCorpsDroite->refresh();
    hitboxPied->refresh();
    hitboxCorpsGauche->refresh();

    if(m_y > 150){
        //qInfo() << m_y;
        ((Scene*) scene())->perdu();
        return;
    }
}

void PiouPiou::updatePixmap(bool right)
{
    QString direction = right ? "Right" : "Left";
    numSprite = (numSprite == 2) ? 1 : 2;

    sprite->setPixmap(QPixmap(QString(SPRITE_PIOUPIOU_PATH).arg(direction).arg(numSprite)));
}

qreal PiouPiou::getXPos()
{
    return sprite->x();
}

qreal PiouPiou::getYPos()
{
    return sprite->y();
}
