#include "../../Headers/Entity/chasseur.h"
#include "../../Headers/scene.h"

#include <QtMath>

Chasseur::Chasseur(qreal startX, qreal startY)
{
    sprite = new QGraphicsPixmapItem(QPixmap(QString(SPRITE_CHASSEUR_PATH).arg(0)));
    addToGroup(sprite);
    m_numSprite = 0;


    scale = 1;
    posYSol = -boundingRect().height() * scale;

    sprite->setScale(scale);
    sprite->setPos(startX, startY + -sprite->boundingRect().height() * scale);

    QTimer* timeDeath = new QTimer(this);
    timeDeath->setInterval(100);
    timeDeath->isSingleShot();
    timeDeath->setSingleShot(true);
    connect(timeDeath, &QTimer::timeout, [=] () {
        death();
    });

    hitboxTete = new HitBox(sprite, 22, 40, 80, 20, "Rebond");
    hitboxTete->setActionneur(timeDeath);
    addToGroup(hitboxTete);
    hitboxCorps = new HitBox(sprite, 22, 70, 80, 60, "Ennemis");
    addToGroup(hitboxCorps);

    sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
    scaleX = -1;

    timerCheckSide = new QTimer(this);
    connect(timerCheckSide, &QTimer::timeout, [=] () {
        if(((Scene*) scene())->getJoueur()->getXPos() > sprite->x() && scaleX == -1){
            scaleX = 1;
            sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
        }else if(((Scene*) scene())->getJoueur()->getXPos() < sprite->x() && scaleX == 1){
            scaleX = -1;
            sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
        }
    });
    timerCheckSide->start(75);

    animationTir = new QPropertyAnimation(this, "numSprite", this);
    animationTir->setStartValue(9);
    animationTir->setEndValue(0);
    animationTir->setEasingCurve(QEasingCurve::Linear);
    animationTir->setDuration(900);

    timerShoot = new QTimer(this);
    connect(timerShoot, &QTimer::timeout, [=] () {
        if( qFabs( sprite->x() - ((Scene*) scene())->getJoueur()->getXPos() ) < 600){
            Balle* newBalle = new Balle(this, sprite->x(), sprite->y(), -scaleX);
            animationTir->start();
        }
    });
    timerShoot->start(3000);


    animationMort = new QPropertyAnimation(this, "numSprite", this);
    animationMort->setStartValue(10);
    animationMort->setEndValue(45);
    animationMort->setEasingCurve(QEasingCurve::Linear);
    animationMort->setDuration(1000);

    connect(animationMort, &QPropertyAnimation::finished, [=] () {
        ((Scene*) scene())->deleteChasseur(this);
        delete this;
    });

}

Chasseur::~Chasseur()
{
    //delete hitboxTete;
    //delete hitboxCorps;
    animationTir->stop();
    animationMort->stop();
    timerShoot->stop();
    timerCheckSide->stop();
}

int Chasseur::numSprite() const
{
    return m_numSprite;
}

void Chasseur::addProjectile(Balle* b)
{
    addToGroup(b);
    tabProjectiles.append(b);
}

void Chasseur::deleteProjectile(Balle* b)
{
    removeFromGroup(b);
    int index = tabProjectiles.indexOf(b);
    if(index >= 0){
        tabProjectiles.removeAt(index);
    }
}

void Chasseur::death()
{
    delete hitboxTete;
    delete hitboxCorps;
    timerShoot->stop();
    timerCheckSide->stop();
    animationTir->stop();
    animationMort->start();
}

void Chasseur::setNumSprite(int numSprite)
{
    numSprite /= 5;
    if(m_numSprite != numSprite){
        m_numSprite = numSprite;
        sprite->setPixmap(QPixmap(QString(SPRITE_CHASSEUR_PATH).arg(numSprite)));
        if(scaleX == -1){
            sprite->setPixmap(sprite->pixmap().transformed(QTransform().scale(-1, 1)));
        }
    }
}
