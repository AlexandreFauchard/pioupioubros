#include "../../Headers/Entity/nuage.h"

#include <QRandomGenerator>
#include <QEasingCurve>
#include <QGraphicsScene>
#include <QDebug>
#include <QTimer>

Nuage::Nuage() :
    sprite(new QGraphicsPixmapItem(QPixmap(SPRITE_NUAGE_PATH)))
{

    scale = 1 - (QRandomGenerator::global()->bounded(20) / 100.0);
    sprite->setScale(scale);

    addToGroup(sprite);

    yPos = 1500 + QRandomGenerator::global()->bounded(300) + (sprite->boundingRect().height()*scale);
    int xPos = QRandomGenerator::global()->bounded(2000 + sprite->boundingRect().width()*scale);

    sprite->setPos(xPos, - yPos);

    vitesseDeplacement = QRandomGenerator::global()->bounded(4) + 1;
    if(vitesseDeplacement >= 3){
        sprite->setPixmap(QPixmap(SPRITE_NUAGE_MAGIQUE_PATH));
    }

    QTimer* timerDeplacement = new QTimer(this);
    connect(timerDeplacement, &QTimer::timeout, [=] () {

        if(x() - vitesseDeplacement < scene()->sceneRect().x() - 50 - sprite->boundingRect().width()*scale){
            m_x = scene()->sceneRect().x() + scene()->sceneRect().width() + sprite->boundingRect().width()*scale + QRandomGenerator::global()->bounded(200);
            yPos = 75 + QRandomGenerator::global()->bounded(300) + (sprite->boundingRect().height()*scale);
            scale = 1 - (QRandomGenerator::global()->bounded(20) / 100);
            sprite->setScale(scale);
            vitesseDeplacement = QRandomGenerator::global()->bounded(4) + 1;
            if(vitesseDeplacement >= 3){
                sprite->setPixmap(QPixmap(SPRITE_NUAGE_MAGIQUE_PATH));
            }else{
                sprite->setPixmap(QPixmap(SPRITE_NUAGE_PATH));
            }
            //setX(x() + 2000);
        }else{
            setX(x() - vitesseDeplacement);
        }
    });
    timerDeplacement->start(15);
}

qreal Nuage::x() const
{
    return m_x;
}



void Nuage::setX(qreal x)
{
    m_x = x;

    sprite->setPos(x, -yPos);
}

Nuage::~Nuage()
{
    //xAnimation->stop();
    scene()->removeItem(this);
}
