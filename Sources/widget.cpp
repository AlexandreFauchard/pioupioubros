#include "../Headers/widget.h"
#include "ui_widget.h"

#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    StartGame();



}

Widget::~Widget()
{
    delete ui;
}

void Widget::StartGame()
{
    scene = new Scene(this);
    scene->setSceneRect(0,-480,955,520);

    ui->graphicsView->setScene(scene);
}

void Widget::RestartGame()
{
    delete scene;
    scene = new Scene(this);
    scene->setSceneRect(0,-480,955,520);

    ui->graphicsView->setScene(scene);
}

