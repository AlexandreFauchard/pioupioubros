# PIOUPIOU Bross

Bonjour ! Voici un super jeu développé avec le framework C++ Qt_.


# Niveaux

Pour l'instant ***Pioupiou Bross*** ne contient qu'un seul niveau

## Niveau 1

Le niveau 1 (unique niveau de ***Pioupiou Bross***) est d'une longueur de 16 500 Pixels.
Voici le schéma de l'ensemble du niveau :
![Niveau 1](https://drive.google.com/open?id=1Gb2MRLzNcXzXSkAcjG7unsVDhxQndd70)
